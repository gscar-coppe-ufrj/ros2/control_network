# Description
Framework to create control networks in ROS2

# License
This Software is distributed under the [MIT License](https://opensource.org/licenses/MIT).
