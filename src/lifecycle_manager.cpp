#include <control_network/lifecycle_manager.hpp>

using namespace control_network;



LifecycleManager::LifecycleManager(rclcpp::Node* node)
{
    node_ = node;
    current_config_ = nullptr;

    auto config = std::make_shared<Config>("none");
    configs_.push_back(config);

    parse_parameters();
}

bool LifecycleManager::Initialize()
{
    std::vector<std::shared_ptr<LifecycleManager::NodeData>> deactivation_list;
    for (auto & config : configs_)
    {
        for (auto node_data : config->nodes_)
        {
            bool found = false;
            for (auto deactivation_list_node_data : deactivation_list)
            {
                if (deactivation_list_node_data->name() == node_data->name())
                {
                    found = true;
                    break;
                }
            }
            if (!found)
                deactivation_list.push_back(node_data);
        }
    }
    if (!deactivate_nodes(deactivation_list))
        return false;

    return activate_nodes(get_node_cfg_list(current_config_));
}

std::shared_ptr<LifecycleManager::Config> LifecycleManager::find_config(std::string config_name)
{
    for (auto & cfg : configs_)
        if (cfg->name() == config_name)
            return cfg;
    return nullptr;
}

void LifecycleManager::parse_parameters()
{
    auto parameters_client = std::make_shared<rclcpp::SyncParametersClient>(node_);
    while (!parameters_client->wait_for_service(std::chrono::seconds(1))) {
      if (!rclcpp::ok()) {
        RCLCPP_ERROR(node_->get_logger(),
            "Interrupted while waiting for the paremeters service. Exiting.");
      }
      RCLCPP_INFO(node_->get_logger(), "Parameters service not available, waiting again...");
    }

    auto parameters_and_prefixes =
        parameters_client->list_parameters(std::vector<std::string>(), 10);

    for (auto & param_name : parameters_and_prefixes.names)
    {
        if (param_name.find("configs.", 0) != 0)
            continue;

        auto config_name_pos_begin = std::string("configs.").size();
        auto config_name_pos_end = param_name.find(".", config_name_pos_begin);
        if (config_name_pos_end == std::string::npos)
            continue;
        auto config_name = param_name.substr(config_name_pos_begin,
            config_name_pos_end - config_name_pos_begin);
        if (config_name == "none")
        {
            RCLCPP_WARN(node_->get_logger(), "Configuration 'none' is being ignored since it's "
            "a reserved keyword.");
            continue;
        }
        auto config = find_config(config_name);
        if (!config)
        {
            config = std::make_shared<Config>(config_name);
            configs_.push_back(config);
        }

        if (param_name == "configs." + config_name + ".nodes")
        {
            rclcpp::Parameter param;
            if (!node_->get_parameter(param_name, param))
                RCLCPP_ERROR(node_->get_logger(), "Couldn't extract parameter %s",
                param_name.c_str());
            auto node_names = param.as_string_array();
            for (auto node_name : node_names)
            {
                auto node_config = std::make_shared<NodeData>(node_name);
                config->nodes_.push_back(node_config);
            }
        }
        else if (param_name == "configs." + config_name + ".parents")
        {
            rclcpp::Parameter param;
            if (!node_->get_parameter(param_name, param))
                RCLCPP_ERROR(node_->get_logger(), "Couldn't extract parameter %s",
                param_name.c_str());
            auto parents = param.as_string_array();
            for (auto & parent : parents)
            {
                auto parent_config = find_config(parent);
                if (!parent_config)
                {
                    parent_config = std::make_shared<Config>(parent);
                    configs_.push_back(parent_config);
                }

                config->parents_.push_back(parent_config);
            }
        }
        else
        {
            auto node_name_end = param_name.find(".", config_name_pos_end + 1);
            auto node_name = param_name.substr(config_name_pos_end + 1,
                node_name_end - config_name_pos_end - 1);
            if (param_name == "configs." + config_name + "." + node_name + ".reactivate")
            {
                rclcpp::Parameter param;
                if (!node_->get_parameter(param_name, param))
                    RCLCPP_ERROR(node_->get_logger(), "Couldn't extract parameter %s",
                    param_name.c_str());
                std::shared_ptr<NodeData> node_config = nullptr;
                for (auto n : config->nodes_)
                {
                    if (n->name() == node_name)
                    {
                        node_config = n;
                        break;
                    }
                }
                if (node_config)
                    node_config->reactivate_ = param.as_bool();
                else
                {
                    node_config = std::make_shared<NodeData>(node_name);
                    node_config->reactivate_ = param.as_bool();
                    config->nodes_.push_back(node_config);
                }
            }
            if (param_name == "configs." + config_name + "." + node_name + ".reconfigure")
            {
                rclcpp::Parameter param;
                if (!node_->get_parameter(param_name, param))
                    RCLCPP_ERROR(node_->get_logger(), "Couldn't extract parameter %s",
                    param_name.c_str());
                std::shared_ptr<NodeData> node_config = nullptr;
                for (auto n : config->nodes_)
                {
                    if (n->name() == node_name)
                    {
                        node_config = n;
                        break;
                    }
                }
                if (node_config)
                    node_config->reconfigure_ = param.as_bool();
                else
                {
                    node_config = std::make_shared<NodeData>(node_name);
                    node_config->reconfigure_ = param.as_bool();
                    config->nodes_.push_back(node_config);
                }
            }
        }
    }

    rclcpp::Parameter initial_config_param;
    if (!node_->get_parameter("initial_config", initial_config_param))
    {
        RCLCPP_WARN(node_->get_logger(), "Couldn't extract parameter initial_config. "
        "Setting the initial configuration as 'none'.");
        current_config_ = find_config("none");
    }
    else
        current_config_ = find_config(initial_config_param.as_string());
    if (!current_config_)
        RCLCPP_ERROR(node_->get_logger(), "Couldn't find initial configuration %s",
        initial_config_param.as_string().c_str());
}

void LifecycleManager::recursive_get_node_cfg_list(
    std::vector<std::shared_ptr<LifecycleManager::NodeData>> & nodes_data,
    std::shared_ptr<LifecycleManager::Config> & config)
{
    for (auto & config_node_data : config->nodes_)
    {
        bool found = false;
        for (auto node_data : nodes_data)
        {
            if (node_data->name() == config_node_data->name())
            {
                found = true;
                break;
            }
        }
        if (!found)
            nodes_data.push_back(config_node_data);
    }
    for (auto & parent : config->parents_)
        recursive_get_node_cfg_list(nodes_data, parent);
}

std::vector<std::shared_ptr<LifecycleManager::NodeData>>
    LifecycleManager::get_node_cfg_list(
    std::shared_ptr<LifecycleManager::Config> config)
{
    std::vector<std::shared_ptr<LifecycleManager::NodeData>> nodes_data;
    recursive_get_node_cfg_list(nodes_data, config);
    return nodes_data;
}

void LifecycleManager::configure_nodes(std::string config_name)
{
    std::vector<std::shared_ptr<LifecycleManager::NodeData>> unconfigure_list;
    auto deactivate_list = get_node_cfg_list(current_config_);
    auto new_config = find_config(config_name);
    auto activate_list = get_node_cfg_list(find_config(config_name));
    std::vector<std::vector<std::shared_ptr<LifecycleManager::NodeData>>::iterator>
        remove_from_deactivate_list;

    for (auto node_ptr : activate_list)
    {
        if (node_ptr->reconfigure_)
            unconfigure_list.push_back(node_ptr);
    }

    for (auto deact_node_config_it = deactivate_list.begin();
        deact_node_config_it != deactivate_list.end(); deact_node_config_it++)
    {
        if ((*deact_node_config_it)->reactivate_ || (*deact_node_config_it)->reconfigure_)
            continue;
        auto act_node_name_it = std::find(begin(activate_list), end(activate_list),
            *deact_node_config_it);
        if (act_node_name_it != activate_list.end())
        {
            activate_list.erase(act_node_name_it);
            remove_from_deactivate_list.insert(remove_from_deactivate_list.begin(),
                deact_node_config_it);
        }
    }
    for (auto it : remove_from_deactivate_list)
        deactivate_list.erase(it);
    unconfigure_nodes(unconfigure_list);
    deactivate_nodes(deactivate_list);
    activate_nodes(activate_list);

    current_config_ = new_config;
}

bool LifecycleManager::change_node_state(
    std::shared_ptr<LifecycleManager::NodeData> node_data,
    lifecycle_msgs::msg::State::_id_type desired_state)
{
    using namespace lifecycle_msgs;

    if (desired_state != msg::State::PRIMARY_STATE_UNCONFIGURED &&
        desired_state != msg::State::PRIMARY_STATE_INACTIVE &&
        desired_state != msg::State::PRIMARY_STATE_ACTIVE &&
        desired_state != msg::State::PRIMARY_STATE_FINALIZED)
    {
        RCLCPP_ERROR(node_->get_logger(), "The desired state of node %s is %u, which is not"
        " a primary state. Giving up on changing the node state", node_data->name(), desired_state);
        return false;
    }
    unsigned int error_counter = 0;

    while (rclcpp::ok() && error_counter < 5)
    {
        auto cb_group = node_->create_callback_group(
            rclcpp::callback_group::CallbackGroupType::MutuallyExclusive);

        // Get state
        node_data->client_get_state_ = node_->create_client<lifecycle_msgs::srv::GetState>(
            node_data->name() + "/get_state", rmw_qos_profile_default, cb_group);
        if (!node_data->client_get_state_->wait_for_service(std::chrono::milliseconds(500)))
        {
            RCLCPP_INFO(node_->get_logger(), "Waiting for get_state service of node %s",
                node_data->name().c_str());
            error_counter++;
            continue;
        }
        auto request_get_state = std::make_shared<srv::GetState::Request>();
        auto result_future_get_state =
            node_data->client_get_state_->async_send_request(request_get_state);
        auto ret_result_future_get_state =
            result_future_get_state.wait_for(std::chrono::duration<double, std::milli>(1000));
        if (ret_result_future_get_state == std::future_status::timeout)
        {
            error_counter++;
            RCLCPP_WARN(node_->get_logger(), "Timeout while waiting for get state for node %s."
                " Error counter = %u", node_data->name().c_str(), error_counter);
            continue;
        }
        if (ret_result_future_get_state == std::future_status::deferred)
        {
            std::cout << "deferred" << std::endl;
            // TODO: How should the deferred status be treated?
        }
        auto result_get_state = result_future_get_state.get();
        if (result_get_state->current_state.id == desired_state)
            return true;

        // Select new state
        auto request_change_state = std::make_shared<srv::ChangeState::Request>();
        switch (result_get_state->current_state.id)
        {
        case msg::State::PRIMARY_STATE_UNCONFIGURED:
            if (desired_state == msg::State::PRIMARY_STATE_INACTIVE ||
                desired_state == msg::State::PRIMARY_STATE_ACTIVE)
                request_change_state->transition.id =
                    msg::Transition::TRANSITION_CONFIGURE;
            if (desired_state == msg::State::PRIMARY_STATE_FINALIZED)
                request_change_state->transition.id =
                    msg::Transition::TRANSITION_UNCONFIGURED_SHUTDOWN;
            break;
        case msg::State::PRIMARY_STATE_INACTIVE:
            if (desired_state == msg::State::PRIMARY_STATE_UNCONFIGURED)
                request_change_state->transition.id =
                    msg::Transition::TRANSITION_CLEANUP;
            if (desired_state == msg::State::PRIMARY_STATE_ACTIVE)
                request_change_state->transition.id =
                    msg::Transition::TRANSITION_ACTIVATE;
            if (desired_state == msg::State::PRIMARY_STATE_FINALIZED)
                request_change_state->transition.id =
                    msg::Transition::TRANSITION_INACTIVE_SHUTDOWN;
            break;
        case msg::State::PRIMARY_STATE_ACTIVE:
            if (desired_state == msg::State::PRIMARY_STATE_UNCONFIGURED ||
                desired_state == msg::State::PRIMARY_STATE_INACTIVE)
                request_change_state->transition.id =
                    msg::Transition::TRANSITION_DEACTIVATE;
            if (desired_state == msg::State::PRIMARY_STATE_FINALIZED)
                request_change_state->transition.id =
                    msg::Transition::TRANSITION_ACTIVE_SHUTDOWN;
            break;
        case msg::State::PRIMARY_STATE_FINALIZED:
            // from this state nothing else can be done
            RCLCPP_ERROR(node_->get_logger(), "The current state of node %s is %u (finilized), and "
                "the desired state is %u. Giving up on changing the node state",
                node_data->name().c_str(), (unsigned int)msg::State::PRIMARY_STATE_FINALIZED,
                (unsigned int)desired_state);
            return false;
            break;  // break has no effect
        default: // PRIMARY_STATE_UNKNOWN or transition states
            // wait for another state to be acquired
            continue;
            break; // break has no effect
        }

        // Change state
        node_data->client_change_state_ = node_->create_client<lifecycle_msgs::srv::ChangeState>(
            node_data->name() + "/change_state", rmw_qos_profile_default, cb_group);
        if (!node_data->client_change_state_->wait_for_service(std::chrono::milliseconds(500)))
        {
            RCLCPP_INFO(node_->get_logger(), "Waiting for change_state service of node %s",
                node_data->name().c_str());
            error_counter++;
            continue;
        }
        auto result_future_change_state =
            node_data->client_change_state_->async_send_request(request_change_state);
        auto ret_result_future_change_state =
            result_future_change_state.wait_for(std::chrono::duration<double, std::milli>(1000));
        if (ret_result_future_change_state == std::future_status::timeout)
        {
            error_counter++;
            RCLCPP_WARN(node_->get_logger(), "Timeout while waiting for change state for node %s."
                " Error counter = %u", node_data->name().c_str(), error_counter);
            continue;
        }
        if (ret_result_future_change_state == std::future_status::deferred)
        {
            std::cout << "deferred" << std::endl;
            // TODO: How should the deferred status be treated?
        }
        auto result_change_state = result_future_change_state.get();
        if (result_change_state->success)
            return true;

        RCLCPP_WARN(node_->get_logger(), "Failed to transition node %s from state %u, to state"
        "%u. Error count = %u", node_data->name(),
        result_get_state->current_state.id, desired_state, error_counter);
        error_counter++;

        if (error_counter > 5)
        {
            RCLCPP_WARN(node_->get_logger(), "The current state of node %s is %u, and the desired"
            "state is %u (finalized).", node_data->name(), result_get_state->current_state.id,
            msg::State::PRIMARY_STATE_FINALIZED);
            continue;
        }
    }

    if (error_counter >= 5)
    {
        RCLCPP_ERROR(node_->get_logger(), "Giving up on changing the node state due to counting %u"
        " errors.", error_counter);
        return false;
    }
    return true;
}

bool LifecycleManager::unconfigure_nodes(
    std::vector<std::shared_ptr<LifecycleManager::NodeData>> nodes)
{
    for (auto node : nodes)
    {
        if (!rclcpp::ok())
            break;
        RCLCPP_INFO(node_->get_logger(), "unconfiguring node %s", node->name().c_str());
        if (!change_node_state(node, lifecycle_msgs::msg::State::PRIMARY_STATE_UNCONFIGURED))
            return false;
    }
    return true;
}

bool LifecycleManager::activate_nodes(
    std::vector<std::shared_ptr<LifecycleManager::NodeData>> nodes)
{
    for (auto node : nodes)
    {
        if (!rclcpp::ok())
            break;
        RCLCPP_INFO(node_->get_logger(), "activating node %s", node->name().c_str());
        if (!change_node_state(node, lifecycle_msgs::msg::State::PRIMARY_STATE_ACTIVE))
            return false;
    }
    return true;
}

bool LifecycleManager::deactivate_nodes(
    std::vector<std::shared_ptr<LifecycleManager::NodeData>> nodes)
{
    for (auto node : nodes)
    {
        if (!rclcpp::ok())
            break;
        RCLCPP_INFO(node_->get_logger(), "deactivating node %s", node->name().c_str());
        if (!change_node_state(node, lifecycle_msgs::msg::State::PRIMARY_STATE_INACTIVE))
            return false;
    }
    return true;
}
