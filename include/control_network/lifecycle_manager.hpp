#pragma once


#include <memory>
#include <string>
#include <vector>
#include <map>
#include <rclcpp/rclcpp.hpp>
#include <lifecycle_msgs/srv/change_state.hpp>
#include <lifecycle_msgs/srv/get_state.hpp>


namespace control_network {


class LifecycleManager
{
    class NodeData
    {
        std::string name_;

    public:
        bool reactivate_;
        bool reconfigure_;

        NodeData(std::string name) : name_(name), reactivate_(false), reconfigure_(false) {}
        std::string name() {return name_;}
        rclcpp::Client<lifecycle_msgs::srv::GetState>::SharedPtr client_get_state_;
        rclcpp::Client<lifecycle_msgs::srv::ChangeState>::SharedPtr client_change_state_;
    };

    class Config
    {
        std::string name_;

    public:
        std::vector<std::shared_ptr<Config>> parents_;
        std::vector<std::shared_ptr<NodeData>> nodes_;

        Config(std::string name) : name_(name) {}
        std::string name() {return name_;};
    };

    std::vector<std::shared_ptr<Config>> configs_;
    std::shared_ptr<Config> current_config_;
    rclcpp::Node* node_;

    std::shared_ptr<LifecycleManager::Config> find_config(std::string config_name);
    void parse_parameters();
    void recursive_get_node_cfg_list(
        std::vector<std::shared_ptr<LifecycleManager::NodeData>> & nodes_data,
        std::shared_ptr<LifecycleManager::Config> & config);
    std::vector<std::shared_ptr<LifecycleManager::NodeData>> get_node_cfg_list(
        std::shared_ptr<LifecycleManager::Config> config);
    bool change_node_state(std::shared_ptr<LifecycleManager::NodeData> node_data,
        lifecycle_msgs::msg::State::_id_type desired_state);
    bool unconfigure_nodes(std::vector<std::shared_ptr<LifecycleManager::NodeData>> nodes);
    bool activate_nodes(std::vector<std::shared_ptr<LifecycleManager::NodeData>> nodes);
    bool deactivate_nodes(std::vector<std::shared_ptr<LifecycleManager::NodeData>> nodes);

public:
    LifecycleManager(rclcpp::Node* node);
    bool Initialize();
    void configure_nodes(std::string config_name);
};


}