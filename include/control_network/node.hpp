#pragma once


#include <rclcpp/time.hpp>


namespace control_network
{


class Node
{
public:
    typedef std::shared_ptr<Node> SharedPtr;

public:
    virtual bool update(const rclcpp::Time& time) = 0;
};


}
